/**
  * @author Oles Bolotniuk ( falll@ukr.net )
  * @inspired by Vladimir Stechenko
  * @special thanks to:
  * 	- Nikolay Savin
  * 	- Vlad Subbotin
  * 	- Denis Davydov
  */ 

// TO STOP RECURSION
var stop = false;

// OUR PLAYER
var player = null;

// HIDE LOADER ON WINDOW READY
$(window).ready(function(){
	loading(0);
})

$(document).ready(function(){

    /**
      * VK SIGN IN BUTTON
      */ 
    $('body').on('click', '.sign-button', function(){

        // SHOW LOADER
        loading(1);

        // TRY TO AUTH AND FETCH RESPONSE
        VK.Auth.login(function(response) {  
            fetchResponse(response) 
        },8); 
    })


    /** 
      *  AUDIO SEARCH BUTTON CLICK
      */ 
    $('body').on('click', '.search-button', function(){

        // SHOW LOADER
        loading(1);

        // SEARCH QUERY FROM INPUT
        var query = $('.search-input').val();

        // LETS SEARCH
        VK.api('audio.search',{q: query, auto_complete:1, lyrics:1},function(data) { 

            // CLEAR RESULTS DIV
            $('.results').html('');
            
            // ALIAS 
            var item = null;

            // GO THROUGH RESULTS
            if(data.response.length>0) {
                for(var i in data.response) {

                    item = data.response[i];

                    if(typeof item == "object") {
                        $('.results').append(
                            "<div class='result-item' data-url='"+item.url+"' data-lyrics='"+item.lyrics_id+"'>"
                            +"<div class='title'>"+item.artist+" — "+item.title+"</div>"
                            +"<div class='text'></div>"
                            +"</div>"
                        );
                    }
                }
            }else{
                $('.results').html('Nothing found')
            }

            // HIDE LOADER
            loading(0);
        });
    })

    /**
      * ON RESULT ITEM CLICK
      */ 
    $('body').on('click', '.result-item .title', function(){

        // TEXT CONTAINER
        var text = $(this).parent().find('.text');

        // IF THERE WAS A LYRICS ALREADY - HIDE IT
        if(text.html().length>0) {
            text.hide();
            text.html('');
            return;
        }

        // SHOW LOADER
        loading(1);

        // LYRICS ID FOR API SEARCH
        var lyrics_id = $(this).parent().data('lyrics');

        // SEARCH FOR LYRICS
        VK.api('audio.getLyrics',{lyrics_id: lyrics_id},function(data) { 

            // FULFILL LYRICS
            text.show();
            text.html("<span>"+data.response.text.replace(/\n/g," <br> ")+"</span>")
            text.append("<button class='lyrix'>Watch a lyrix!</button>")
        
            // FINALLY HIDE LOADER
            loading(0);
        })

    })

    /**
      * WATCH A LYRIX BUTTON
      */
    $('body').on('click', '.result-item .text .lyrix', function(){

        // FETCH LYRIX
        fetchLyrics($(this).parent().find('span').html())

        // SHOW PLAYER AND FILL IT WITH SOUND ^_^
        $('.player').show();
        $('#player').attr('src',$(this).closest('.result-item').data('url'));
        
        if(!player)
        	player = new MediaElementPlayer('#player');
    })

    /**
      * MANUAL LYRICS INPUT BUTTON
      */ 
    $('body').on('click', '.manual-button', function(){

        // HIDE PLAYER CUZ WE DONT NEED IT
        $('.player').hide();

        // GET LYRICS FROM TEXTAREA
        var lyrics = $('.manual-lyrics').val();

        // PREVENT EMPTY FETCHING
        if(lyrics.length<=0) 
            return;

        // FETHC LYRIX!
        fetchLyrics(lyrics);
    })

    /**
      * HANDLE WINDOW SCROLL
      */ 
	$(window).scroll(function(){

		// CURRENT SCROLL OFFSET
		var scroll = $('body').scrollTop();

		// OUR PLAYER DIV
		var player = $('.player');

		// MAGIC OFFSET
		var magic = 500;

		// IF PLAYER ISNT VISIBLE ANYMORE
		if(scroll>magic && player.is(':visible')) {
			player.css('position','fixed')
			player.css('left','68%')
			player.css('top','90%')
			player.css('border','2px solid #ccc')

			// SHOW LEFT PANEL
			$('.back-to-top').show()
		}else{
			player.css('position','relative')
			player.css('top','0%')
			player.css('left','0%')
			player.css('border','0px')

			// HIDE LEFT PANEL
			$('.back-to-top').hide()
		}
	})

	/**
	  * BACK TO TOP LEFT PANEL CLICK
	  */
	$('body').on('click','.back-to-top', function(){
		$('html,body').animate({ scrollTop: 0 }, 500);
	})

	/**
	  * BACK BUTTON
	  */
	$('body').on('click', '.back-button', function(e){

		// SHOW LOADER
		loading(1);

		// STOP RECURSION
		stop = true;

		// SHOW SEARCH BOX
		$('.main-box').fadeOut(function(){
			$('.search-box').fadeIn(function(){
				loading(0)
			})
		});
	})

	/** 
	  * ON SEARCH INPUT ENTER CLICK - SUBMIT
	  */
	$('body').on('keypress', '.search-input', function(e){
		if(e.keyCode==13)
			$('.search-button').click();
	})

	/**
	  * ON LOGO CLICK = SHOW SECRET IMAGE
	  */ 
	$('.logo').on('click', function(){
		$('.secret').fadeIn("slow",function(){
			$('.secret').fadeOut('slow');
		});
	})
})

/**
  * FETCH GIVEN LYRICS, CLEAR THEM, SPLIT INTO GROUPS
  */ 
function fetchLyrics(lyrics){

	// SHOW LOADER
	loading(1);

	// SHOW LYRICS BOX
	$('.search-box').fadeOut(function(){
		$('.main-box').fadeIn()
	});

	// REPLACE BR's
	lyrics = lyrics.replace(/<br>/g,"");

	// REPLACE ALL THAT ISNT A WORD
	lyrics = lyrics.replace(/[^A-zА-яэъыіє\s]/gi, '');

	// REPLACE MORE THAN 2 SPACES WITH ONE SPACE
	lyrics = lyrics.replace(/\s{2,}/g," ");

	// SPLIT LYRICS INTO ARRAY
	lyrics = lyrics.split(" ");

	// ARRAY FOR CLEAN LYRICS
	var clean = [];

	// SEND ONLY TRIMMED, WORDS! TO CLEAN LYRICS
	for(var i in lyrics){
		if(/[A-zА-яЄIЫЪЭ]+/.test(lyrics[i]))
			clean.push( lyrics[i].trim() )
	}

	var groups = [];
	var random = 0;
	var group = "";

	// GO THROUGH CLEAN LYRICS
	for(var i=0; i<clean.length; i++){

		// GET RANDOM NUM OF WORDS
		random = getRand(2,3);

		// ALWAYS CLEAR VARIABLES!
		group 	= "";
		sep 	= "";

		for(j=0;j<random;j++){
			if(clean[i+j]) {
				group += sep + clean[i+j];
				sep = " ";
			}
		}

		// OFFSET TO NUM OF WORDS WE HAVE
		i+= random-1;

		// PUSH PIECE OF LYRIC INTO GROUPS
		groups.push(group);
	}

	// CLEAR DIV
	$(".main-box .images").html('');

	// GET ALL IMAGES FOR LYRICS
	stop = false;
	getImages(groups, 0);
}

/**
  * GET ALL IMAGES FOR GROUPS
  */ 
function getImages(lyrics, i) {

	// RETURN IF NO MORE LYRICS
	if(!lyrics[i] || stop) return;

	// TAKE IMAGE FROM GOOGOL
	$.getJSON("https://ajax.googleapis.com/ajax/services/search/images?callback=?",{q:lyrics[i],v:'1.0'},function(data){

		// GET SRC
		try{
			var src = data.responseData.results[getRand(0,data.responseData.results.length-1)].url;
		}catch(e){
			var src = data.responseData.results[0].url;
			alert('src fails! now its '+ src)
		}

		// APPEND AN IMAGE
		$(".main-box .images").append(
			'<div class="block">'
			+'<div class="text">'+lyrics[i]+'</div>'
			//+'<br><img src="img/bracket.png"></div>'
			+'<img class="image" id="image-'+i+'" data-query="'+lyrics[i]+'" onerror="reloadMe(this)" src="'+src+'">'
			+'</div>'
		);
		
		// HIDE LOADER
		loading(0)

		// RECURSIVE CALL
		// GET OTHER IMAGES
		getImages(lyrics,++i)
	})
} 

/**
  * RELOAD ITEM THAT FAILS TO LOAD
  */ 
function reloadMe(item){

	// SEARCH QUERY
	var query = $(item).data('query');

	console.log('replacing '+query)

	// GET NEW IMAGE BY QUERY
	$.getJSON("https://ajax.googleapis.com/ajax/services/search/images?callback=?",{q:query,v:'1.0'},function(data){
		// REPLACE IT WITH NEW
		$(item).parent().html('<div class="text">'+query+'</div> <img class="image" data-query="'+query+'" onerror="reloadMe(this)" src="'+data.responseData.results[getRand(0,data.responseData.results.length-1)].url+'">');
	})	
}

/**
  * RANDOM NUMBER HELPER
  */ 
function getRand(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
  * LOADER HELPER
  */ 
function loading(on){

	var opacity = '0.4';
	var display = 'block'
	if(!on) {
		opacity = '1';
		display = 'none';
	}

	$('.wrapper').css('opacity',opacity)
	$('#loader-wrapper').css('display',display)
}

/**
  * FETCH VK RESPONSE
  */ 
function fetchResponse(response){

	// IF CONNECTED - SHOW SEARCH PANEL
    if(response.status=='connected') {
        $('.sign').parent().hide();
        $('.search').parent().show();
        loading(0)
    }else{
        $('.sign').parent().show();
        $('.search').parent().hide();
    }
}